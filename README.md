**This is a demo project that uses the public api available through https://developers.webcams.travel/**

The purpose of this project is to explore the api and experiment with a fresh Kotlin implementation along with Retrofit library.

---

## API Overview

The webcams.travel API gives you access to largest repository of webcams worldwide. You can filter them by different criteria such as country, category, near a position, and many more. Each webcam has information about its location, preview images, timelapses, and others.

The webcams.travel REST API is only available via Mashape/RapidAPI. 
*In order to access the API, you need a RapidAPI account. Sign up here: https://rapidapi.com/signup

This is a RESTful API. The request format is REST and the responses are formatted in JSON. Everything is UTF-8 encoded.

---

## Links

https://developers.webcams.travel/#webcams/migration-guide
https://developers.webcams.travel/#webcams/examples
https://square.github.io/retrofit/

---
