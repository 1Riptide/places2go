package com.caseystalnaker.places2go

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

class Places2GoApplication : Application() {

    @Override
    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this);
    }

    companion object {
        internal const val mCamAPIBaseURL = "https://webcamstravel.p.mashape.com/"
        internal const val mMashapeKey = "1UmFSzTMvXmshGHCGnNlLyTYQvsPp1fsBdDjsnbeqV0QySqGYR"
        internal const val mMashapeHost = "webcamstravel.p.mashape.com"
    }
}