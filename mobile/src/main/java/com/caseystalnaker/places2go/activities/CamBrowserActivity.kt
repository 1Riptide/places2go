package com.caseystalnaker.places2go.activities

import android.net.Uri
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.caseystalnaker.places2go.Places2GoApplication.Companion.mMashapeHost
import com.caseystalnaker.places2go.Places2GoApplication.Companion.mMashapeKey
import com.caseystalnaker.places2go.R
import com.caseystalnaker.places2go.dto.WebcamDTO
import com.caseystalnaker.places2go.dto.WebcamListDTO
import com.caseystalnaker.places2go.dto.WebcamResponseDTO
import com.caseystalnaker.places2go.dto.WebcamTypeDTO
import com.caseystalnaker.places2go.fragments.CamDrawerFragment
import com.caseystalnaker.places2go.fragments.CameraGridFragment
import com.caseystalnaker.places2go.fragments.VideoViewFragment
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Response

class CamBrowserActivity : CamAPIActivity(),
        CamDrawerFragment.NavigationDrawerCallbacks,
        VideoViewFragment.OnFragmentInteractionListener {

    /**
     * Callback for when an http response for a camera list is received
     * and a parsed as a "dto" object.
     */
    override fun onCamListResponseReady(@Nullable response: Response<WebcamResponseDTO>?) {
        val parsedResponse: Response<WebcamResponseDTO>? = response
        parsedResponse?.body()?.result?.webcams?.let {
            val result: WebcamListDTO? = parsedResponse.body()?.result
            val total: Int? = result?.total

            if (total == null || total == 0) {
                Toast.makeText(this, "No Webcams active for this category", Toast.LENGTH_SHORT).show()
                return@let
            }
            val webcamStreamList: Array<WebcamDTO> = result.webcams!!
            Log.i(TAG, "onCamListResponseReady offset: " + result.offset)
            Log.i(TAG, "onCamListResponseReady total: " + result.total)
            val streamList: ArrayList<WebcamDTO> = ArrayList()
            webcamStreamList.toCollection(streamList)
            mWebcamStreamList?.clear()
            mWebcamStreamList = streamList
            updateUI()
        } ?: Toast.makeText(this, "No Webcams active for this category", Toast.LENGTH_SHORT).show()
    }

    override fun onFragmentInteraction(uri: Uri) {
        Log.d(TAG, "onFragmentInteraction: uri = $uri")
    }

    fun getCamListData(): ArrayList<WebcamDTO>? {
        return mWebcamStreamList
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mWebcamCategoryList = intent.getParcelableArrayListExtra<WebcamTypeDTO>(WEBCAM_CAT_DATA_KEY)
        mWebcamStreamList = intent.getParcelableArrayListExtra<WebcamDTO>(WEBCAM_STREAM_DATA_KEY)

        mTitle = title
        // Set up the drawer.
        mWebcamCategoryList?.let {
            val navigationDrawer = navigation_drawer as CamDrawerFragment
            navigationDrawer.setUp(
                    R.id.navigation_drawer,
                    drawer_layout as DrawerLayout, it)
        }

        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        val existingFragment: Fragment? = supportFragmentManager.findFragmentByTag(CameraGridFragment.TAG)
        if (existingFragment == null) {
            transaction.add(R.id.container, CameraGridFragment.newInstance(), CameraGridFragment.TAG)
            transaction.commit()
        } else {
            transaction.replace(R.id.container, existingFragment)
        }
    }

    override fun onNavigationDrawerItemSelected(position: Int) {

        val catCount: Int? = mWebcamCategoryList?.size
        if (position != 0 && position <= catCount!!) {
            val listLabel: String? = mWebcamCategoryList?.get(position-1)?.name
            listLabel?.let {
                mTitle = listLabel
                fetchCamListByCat(mTitle as String)
            }?: Log.w(TAG, "onNavigationDrawerItemSelected - could not find label. ")
        } else {
            mTitle = getString(R.string.label_all_webcams)
            fetchCamList()
            Log.w(TAG, "onNavigationDrawerItemSelected - out of range")
        }
    }

    /**
     * Retrieve a webcam list by category name.
     * Available categories see {@link
     */
    private fun fetchCamListByCat(@NonNull label: String) {
        // Step 2: fetch cam list
        val camListApiCall: retrofit2.Call<WebcamResponseDTO> = mCamAPI.getWebcamsByCategory(mMashapeKey, mMashapeHost, label, "en", "webcams:image,location,url,user,map,player,property,statistics")
        Log.d(TAG, "fetchCamListByCat(): Request = " + camListApiCall.request())
        super.fetchCamList(camListApiCall)
    }

    private fun fetchCamList() {
        // Step 2: fetch cam list
        val camListApiCall: retrofit2.Call<WebcamResponseDTO> = mCamAPI.getWebcamList(mMashapeKey, mMashapeHost, "en", "webcams:image,location,url,user,map,player,property,statistics")
        Log.d(TAG, "fetchCamList(): Request = " + camListApiCall.request())
        super.fetchCamList(camListApiCall)
    }

    private fun restoreActionBar() {
        val actionBar = supportActionBar as ActionBar
        actionBar.setDisplayShowTitleEnabled(true)
        actionBar.title = mTitle
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setHomeButtonEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val navigationDrawer = navigation_drawer as CamDrawerFragment
        if (!navigationDrawer.isDrawerOpen) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            restoreActionBar()
            return true
        }
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Stub. Will be useful. TODO: prove it.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Optional.
        return super.onOptionsItemSelected(item)
    }

    fun onVideoSelected(dto: WebcamDTO) {
        Toast.makeText(this,
                "Clicked:webcamDTO.id = \n" + dto.title,
                Toast.LENGTH_SHORT).show()

        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.container, VideoViewFragment.newInstance(dto))
        ft.addToBackStack(VideoViewFragment::class.java.simpleName)
        ft.commit()
    }

    fun updateUI() {
        run {

            val cameraGridFragment = supportFragmentManager.findFragmentByTag(CameraGridFragment.TAG) as CameraGridFragment
            mWebcamStreamList?.let(cameraGridFragment::refreshData)
        }
    }

    companion object {

        const val TAG: String = "CamBrowserActivity"

        @JvmStatic
        val WEBCAM_STREAM_DATA_KEY = "webcam.data.key"
        @JvmStatic
        val WEBCAM_CAT_DATA_KEY = "webcam.category.key"

        /**
         * Used to store the last screen title. For use in [.restoreActionBar].
         */
        private var mTitle: CharSequence? = null
        // List of webcam stream data
        private var mWebcamStreamList: ArrayList<WebcamDTO>? = null
        // List of webcam categories
        private var mWebcamCategoryList: ArrayList<WebcamTypeDTO>? = null

    }
}


