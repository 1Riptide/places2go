package com.caseystalnaker.places2go.activities

import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import com.caseystalnaker.places2go.R
import com.caseystalnaker.places2go.fragments.CamDrawerFragment
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private fun restoreActionBar() {
        val actionBar = supportActionBar as ActionBar
        actionBar.setDisplayShowTitleEnabled(true)
        actionBar.title = mTitle
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setHomeButtonEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val navigationDrawer = navigation_drawer as CamDrawerFragment
        if (!navigationDrawer.isDrawerOpen) {
            restoreActionBar()
            return true
        }
        return super.onCreateOptionsMenu(menu)
    }

    companion object {

        const val TAG: String = "HomeActivity"

        /**
         * Used to store the last screen title. For use in [.restoreActionBar].
         */
        private var mTitle: CharSequence? = null

    }
}


