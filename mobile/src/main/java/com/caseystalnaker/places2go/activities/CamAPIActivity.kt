package com.caseystalnaker.places2go.activities

import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.caseystalnaker.places2go.Places2GoApplication.Companion.mCamAPIBaseURL
import com.caseystalnaker.places2go.Places2GoApplication.Companion.mMashapeHost
import com.caseystalnaker.places2go.Places2GoApplication.Companion.mMashapeKey
import com.caseystalnaker.places2go.dto.WebcamResponseDTO
import com.caseystalnaker.places2go.dto.WebcamTypeDTO
import com.caseystalnaker.places2go.interfaces.WebcamAPIInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Superclass for all things related to webcams.travel API
 * https://developers.webcams.travel/
 */
abstract class CamAPIActivity : AppCompatActivity() {


    internal lateinit var mRetrofit: Retrofit
    internal lateinit var mCamAPI: WebcamAPIInterface
    abstract fun onCamListResponseReady(response: Response<WebcamResponseDTO>?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mCamAPI = getAPIInstance()
    }

    private fun getClient(baseUrl: String): Retrofit {
        //http://square.github.io/retrofit/
        mRetrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        return mRetrofit
    }

   private fun getAPIInstance(): WebcamAPIInterface {

       mRetrofit = getClient(mCamAPIBaseURL)
       mCamAPI = mRetrofit.create(WebcamAPIInterface::class.java)
        return mCamAPI
    }

    fun fetchCamList(@NonNull apiCall:retrofit2.Call<WebcamResponseDTO>) {
        apiCall.enqueue(object : Callback<WebcamResponseDTO> {
            override fun onFailure(call: Call<WebcamResponseDTO>?, t: Throwable?) {
                Log.i(TAG, "fetchCamList:onFailure: data = " + t.toString())
            }

            override fun onResponse(call: Call<WebcamResponseDTO>?, response: Response<WebcamResponseDTO>?) {
                Log.i(TAG, "fetchCamList:onResponse: data = " + response.toString())
                onCamListResponseReady(response)
            }
        })
    }

    fun fetchCategoryList(@NonNull apiCall:retrofit2.Call<WebcamResponseDTO>) {
        apiCall.enqueue(object : Callback<WebcamResponseDTO> {
            override fun onFailure(call: Call<WebcamResponseDTO>?, t: Throwable?) {
                Log.i(TAG, "fetchCategoryList:onFailure: data = " + t.toString())
            }

            override fun onResponse(call: Call<WebcamResponseDTO>?, response: Response<WebcamResponseDTO>?) {
                Log.i(TAG, "fetchCategoryList:onResponse: data = " + response.toString())
                updateCamListCategories(response)
            }
        })
    }

    private fun updateCamListCategories(response: Response<WebcamResponseDTO>?){
        // Step 1 complete: record cam categories
        mCamCatList = response?.body()?.result?.categories!!

        // Step 2: fetch cam list
        val camListApiCall: retrofit2.Call<WebcamResponseDTO> = mCamAPI.getWebcamList(mMashapeKey, mMashapeHost,"en", "webcams:image,location,url,user,map,player,property,statistics" )
        fetchCamList(camListApiCall)
    }

    companion object {
        private const val TAG = "CamAPIActivity"
        public lateinit var mCamCatList: Array<WebcamTypeDTO>
    }
}
