package com.caseystalnaker.places2go.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.caseystalnaker.places2go.adapters.SimpleAdapter
import com.caseystalnaker.places2go.decorators.DividerDecoration

class VerticalFragment : RecyclerFragment() {

    override val layoutManager: RecyclerView.LayoutManager
        get() = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

    override//We must draw dividers ourselves if we want them in a list
    val itemDecoration: RecyclerView.ItemDecoration
        get() = DividerDecoration(activity!!)

    override val defaultItemCount: Int
        get() = 100

    override val adapter: SimpleAdapter
        get() = SimpleAdapter()

    companion object {

        fun newInstance(): VerticalFragment {
            val fragment = VerticalFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
