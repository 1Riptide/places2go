package com.caseystalnaker.places2go.fragments

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import com.caseystalnaker.places2go.adapters.SimpleAdapter
import com.caseystalnaker.places2go.decorators.InsetDecoration
import com.caseystalnaker.places2go.layout.FixedGridLayoutManager

class FixedTwoWayFragment : RecyclerFragment() {

    override val layoutManager: RecyclerView.LayoutManager
        get() {
            val manager = FixedGridLayoutManager()
            manager.setTotalColumnCount(10)

            return manager
        }

    override val itemDecoration: RecyclerView.ItemDecoration
        get() = InsetDecoration(activity!!)

    override val defaultItemCount: Int
        get() = 5

    override val adapter: SimpleAdapter
        get() = SimpleAdapter()

    companion object {

        fun newInstance(): FixedTwoWayFragment {
            val fragment = FixedTwoWayFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
