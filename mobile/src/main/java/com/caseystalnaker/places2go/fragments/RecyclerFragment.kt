package com.caseystalnaker.places2go.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.AdapterView
import android.widget.Toast
import com.caseystalnaker.places2go.R
import com.caseystalnaker.places2go.adapters.SimpleAdapter
import com.caseystalnaker.places2go.dialogs.NumberPickerDialog

abstract class RecyclerFragment : Fragment(), AdapterView.OnItemClickListener {

    private var mList: RecyclerView? = null
    private var mAdapter: SimpleAdapter? = null
    private var dialog: NumberPickerDialog? = null

    /** Required Overrides for Sample Fragments  */

    protected abstract val layoutManager: RecyclerView.LayoutManager
    protected abstract val itemDecoration: RecyclerView.ItemDecoration
    protected abstract val defaultItemCount: Int
    protected abstract val adapter: SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_recycler, container, false)

        mList = rootView.findViewById<View>(R.id.section_list) as RecyclerView
        mList!!.layoutManager = layoutManager
        mList!!.addItemDecoration(itemDecoration)

        mList!!.itemAnimator.addDuration = 1000
        mList!!.itemAnimator.changeDuration = 1000
        mList!!.itemAnimator.moveDuration = 1000
        mList!!.itemAnimator.removeDuration = 1000

        mAdapter = adapter
        mAdapter!!.itemCount = defaultItemCount
        mAdapter!!.setOnItemClickListener(this)
        mList!!.adapter = mAdapter

        return rootView
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.grid_options, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {

            /*R.id.action_add -> {
                dialog?.setTitle("Position to Add")
                dialog?.setPickerRange(0, mAdapter!!.itemCount)
                dialog?.setOnNumberSelectedListener(object : NumberPickerDialog.OnNumberSelectedListener {
                    override fun onNumberSelected(value: Int) {
                        mAdapter!!.addItem(value)
                    }
                })
                dialog?.show()

                return true
            }
            R.id.action_remove -> {
                dialog?.setTitle("Position to Remove")
                dialog?.setPickerRange(0, mAdapter!!.itemCount - 1)
                dialog?.setOnNumberSelectedListener(object : NumberPickerDialog.OnNumberSelectedListener {
                    override fun onNumberSelected(value: Int) {
                        mAdapter!!.removeItem(value)
                    }
                })
                dialog?.show()

                return true
            }*/

            R.id.action_empty -> {
                mAdapter!!.itemCount = 0
                return true
            }
            R.id.action_small -> {
                mAdapter!!.itemCount = 5
                return true
            }
            R.id.action_medium -> {
                mAdapter!!.itemCount = 25
                return true
            }
            R.id.action_large -> {
                mAdapter!!.itemCount = 196
                return true
            }
            R.id.action_scroll_zero -> {
                mList!!.scrollToPosition(0)
                return true
            }
            R.id.action_smooth_zero -> {
                mList!!.smoothScrollToPosition(0)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
        Toast.makeText(activity,
                "Clicked: " + position + ", index " + mList!!.indexOfChild(view),
                Toast.LENGTH_SHORT).show()
    }
}
