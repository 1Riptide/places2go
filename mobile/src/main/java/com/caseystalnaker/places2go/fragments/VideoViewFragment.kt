package com.caseystalnaker.places2go.fragments

import android.content.Context
import android.media.MediaPlayer
import android.media.MediaPlayer.OnCompletionListener
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import com.caseystalnaker.places2go.activities.CamBrowserActivity.Companion.TAG
import com.caseystalnaker.places2go.R
import com.caseystalnaker.places2go.dto.WebcamDTO
import kotlinx.android.synthetic.main.video_player_layout.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [VideoViewFragment.OnFragmentInteractionListener] interface to handle interaction events.
 * Use the [VideoViewFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class VideoViewFragment : Fragment() {
    private var webcamDTO: WebcamDTO? = null
    private var listener: OnFragmentInteractionListener? = null
    private var mediacontroller: MediaController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            webcamDTO = it.getParcelable(WEBCAM_DTO)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.video_player_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        progress_bar.visibility = View.VISIBLE
        val videoURI = webcamDTO?.let{
            it.player?.live?.embed
        }
        Log.d(TAG, "onViewCreated: videoURI =" + videoURI)
        mediacontroller = MediaController(getContext())
        mediacontroller!!.setAnchorView(video_view)
        videoURI?.let {
            //val uri = Uri.parse(videoURI)
            val uri = Uri.parse("https://stream.webcams.travel/1485691420")
            video_view.setMediaController(mediacontroller)
            video_view.setVideoURI(uri)
            video_view.requestFocus()
            video_view.start()

            video_view.setOnCompletionListener(object : OnCompletionListener {
                @Override
                override fun onCompletion(mp: MediaPlayer) {
                    /*
                    if (isContinuously) {
                        video_view.start()
                    }
                    */
                }
            })
            video_view.setOnPreparedListener(object: MediaPlayer.OnPreparedListener {
                override fun onPrepared(p0: MediaPlayer?) {
                    progress_bar.visibility = View.GONE
                }

            })

        }?: Log.d(TAG, webcamDTO?.title + ":: No live video.")

    }


    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        @JvmStatic
        val WEBCAM_DTO = "webcam.dto"

        /**
         * Factory method to create a new instances of
         * this fragment using the provided parameters.
         *
         * @param param1 instance of a WebcamDTO Parcelable
         * @return A new instance of fragment VideoViewFragment.
         */
        @JvmStatic
        fun newInstance(param1: WebcamDTO) =
                VideoViewFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(WEBCAM_DTO, param1)
                    }
                }
    }
}
