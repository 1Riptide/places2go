package com.caseystalnaker.places2go.fragments

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import com.caseystalnaker.places2go.adapters.SimpleAdapter
import com.caseystalnaker.places2go.decorators.InsetDecoration

class VerticalStaggeredGridFragment : RecyclerFragment() {

    override val layoutManager: RecyclerView.LayoutManager
        get() = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

    override val itemDecoration: RecyclerView.ItemDecoration
        get() = InsetDecoration(activity!!)

    override val defaultItemCount: Int
        get() = 100

    override val adapter: SimpleAdapter
        get() = SimpleAdapter()

    companion object {

        fun newInstance(): VerticalStaggeredGridFragment {
            val fragment = VerticalStaggeredGridFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
