package com.caseystalnaker.places2go.fragments

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import com.caseystalnaker.places2go.adapters.SimpleAdapter
import com.caseystalnaker.places2go.decorators.InsetDecoration

class VerticalGridFragment : RecyclerFragment() {

    override val layoutManager: RecyclerView.LayoutManager
        get() = GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false)

    override val itemDecoration: RecyclerView.ItemDecoration
        get() = InsetDecoration(activity!!)

    override val defaultItemCount: Int
        get() = 100

    override val adapter: SimpleAdapter
        get() = SimpleAdapter()

    companion object {

        fun newInstance(): VerticalGridFragment {
            val fragment = VerticalGridFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
