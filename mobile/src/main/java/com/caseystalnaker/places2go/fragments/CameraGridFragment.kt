package com.caseystalnaker.places2go.fragments

import android.support.annotation.Nullable
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.View
import android.widget.AdapterView
import com.caseystalnaker.places2go.activities.CamBrowserActivity
import com.caseystalnaker.places2go.decorators.InsetDecoration
import com.caseystalnaker.places2go.dto.WebcamDTO

class CameraGridFragment : CamRecyclerFragment() {

    override val layoutManager: RecyclerView.LayoutManager
        get() = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

    override val itemDecoration: RecyclerView.ItemDecoration
        get() = InsetDecoration(activity!!)

    override fun onItemClick(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
        val webcamDTO: WebcamDTO = mAdapter.getDataAtPosition(position);
        (activity as CamBrowserActivity).onVideoSelected(webcamDTO)
    }

    fun refreshData(@Nullable camList: ArrayList<WebcamDTO>) {

        mAdapter.swap(camList)
    }

    companion object {

        const val TAG: String = "CameraGridFragment";

        fun newInstance(): CameraGridFragment {
            val fragment = CameraGridFragment()
            return fragment
        }
    }
}
