package com.caseystalnaker.places2go.fragments

import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.AdapterView
import com.caseystalnaker.places2go.activities.CamBrowserActivity
import com.caseystalnaker.places2go.R
import com.caseystalnaker.places2go.adapters.WebcamsListAdapter
import com.caseystalnaker.places2go.dto.WebcamDTO
import kotlinx.android.synthetic.main.fragment_recycler.*

abstract class CamRecyclerFragment : Fragment(), AdapterView.OnItemClickListener {
    protected lateinit var mAdapter: WebcamsListAdapter
    protected abstract val layoutManager: RecyclerView.LayoutManager
    protected abstract val itemDecoration: RecyclerView.ItemDecoration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recycler, container, false)
    }

    @Suppress("DEPRECATION")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        section_list.layoutManager = layoutManager
        section_list.addItemDecoration(itemDecoration)

        section_list.itemAnimator.addDuration = 1000
        section_list.itemAnimator.changeDuration = 1000
        section_list.itemAnimator.moveDuration = 1000
        section_list.itemAnimator.removeDuration = 1000


        val webcamStreamListData: ArrayList<WebcamDTO> = (activity as CamBrowserActivity).getCamListData() as ArrayList<WebcamDTO>
        val primaryColorRes = resources.getColor(R.color.colorPrimary)
        val accentColorRes = resources.getColor(R.color.colorAccent)
        webcamStreamListData.let {
            mAdapter = WebcamsListAdapter(it, primaryColorRes, accentColorRes)
            mAdapter.setOnItemClickListener(this)
            section_list.adapter = mAdapter
        }
    }

    override fun onCreateOptionsMenu(@NonNull menu: Menu, @NonNull inflater: MenuInflater) {
        inflater.inflate(R.menu.grid_options, menu)
    }

    override fun onOptionsItemSelected(@NonNull item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_scroll_zero -> {
                section_list.scrollToPosition(0)
                true
            }
            R.id.action_smooth_zero -> {
                section_list.smoothScrollToPosition(0)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
