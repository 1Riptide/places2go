package com.caseystalnaker.places2go.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.caseystalnaker.places2go.adapters.SimpleAdapter
import com.caseystalnaker.places2go.decorators.InsetDecoration

class HorizontalFragment : RecyclerFragment() {

    override val layoutManager: RecyclerView.LayoutManager
        get() = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

    override val itemDecoration: RecyclerView.ItemDecoration
        get() = InsetDecoration(activity!!)

    override val defaultItemCount: Int
        get() = 40

    override val adapter: SimpleAdapter
        get() = SimpleAdapter()

    companion object {

        fun newInstance(): HorizontalFragment {
            val fragment = HorizontalFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
