package com.caseystalnaker.places2go.dialogs

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.widget.NumberPicker


class NumberPickerDialog : AlertDialog, DialogInterface.OnClickListener {

    private val mPicker: NumberPicker by lazy { NumberPicker(context) }
    private var mNumberSelectedListener: OnNumberSelectedListener? = null

    interface OnNumberSelectedListener {
        fun onNumberSelected(value: Int)
    }

    constructor(context: Context) : super(context)

    protected constructor(context: Context, theme: Int) : super(context, theme) {}

    protected constructor(context: Context, cancelable: Boolean, cancelListener: DialogInterface.OnCancelListener) : super(context, cancelable, cancelListener) {}

    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(android.R.string.cancel), this)
        setButton(DialogInterface.BUTTON_POSITIVE, context.getString(android.R.string.ok), this)
        setView(mPicker)
    }

    fun setOnNumberSelectedListener(listener: OnNumberSelectedListener) {
        mNumberSelectedListener = listener
    }

    fun setPickerRange(minValue: Int, maxValue: Int) {
        mPicker.minValue = minValue
        mPicker.maxValue = maxValue
    }

    override fun onClick(dialog: DialogInterface, which: Int) {
        if (which == DialogInterface.BUTTON_POSITIVE && mNumberSelectedListener != null) {
            mNumberSelectedListener!!.onNumberSelected(mPicker.value)
        }
    }
}
