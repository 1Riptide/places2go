package com.caseystalnaker.places2go.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import com.caseystalnaker.places2go.R
import java.util.*

open class SimpleAdapter : RecyclerView.Adapter<SimpleAdapter.VerticalItemHolder>() {

    private val mItems: ArrayList<AdapterItem>

    private var mOnItemClickListener: AdapterView.OnItemClickListener? = null

    init {
        mItems = ArrayList()
    }

    /*
     * A common adapter modification or reset mechanism. As with ListAdapter,
     * calling notifyDataSetChanged() will trigger the RecyclerView to update
     * the view. However, this method will not trigger any of the RecyclerView
     * animation features.
     */
    fun setItemCount(count: Int) {
        mItems.clear()
        mItems.addAll(generateDummyData(count))

        notifyDataSetChanged()
    }

    /*
     * Inserting a new item at the head of the list. This uses a specialized
     * RecyclerView method, notifyItemInserted(), to trigger any enabled item
     * animations in addition to updating the view.
     */
    fun addItem(position: Int) {
        if (position > mItems.size) return

        mItems.add(position, generateDummyItem())
        notifyItemInserted(position)
    }

    /*
     * Inserting a new item at the head of the list. This uses a specialized
     * RecyclerView method, notifyItemRemoved(), to trigger any enabled item
     * animations in addition to updating the view.
     */
    fun removeItem(position: Int) {
        if (position >= mItems.size) return

        mItems.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(container: ViewGroup, viewType: Int): VerticalItemHolder {
        val inflater = LayoutInflater.from(container.context)
        val root = inflater.inflate(R.layout.double_label_item, container, false)

        return VerticalItemHolder(root, this)
    }

    override fun onBindViewHolder(itemHolder: VerticalItemHolder, position: Int) {
        val item = mItems[position]

        itemHolder.setItem2Value(item.item2Value.toString())
        itemHolder.setItem1Value(item.item1Value.toString())

        itemHolder.setItem2Label(item.item2Label)
        itemHolder.setItem1Label(item.item1Label)
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    fun setOnItemClickListener(onItemClickListener: AdapterView.OnItemClickListener) {
        mOnItemClickListener = onItemClickListener
    }

    private fun onItemHolderClick(itemHolder: VerticalItemHolder) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener!!.onItemClick(null, itemHolder.itemView,
                    itemHolder.adapterPosition, itemHolder.itemId)
        }
    }

    class AdapterItem(var item1Label: String, var item2Label: String, var item1Value: Int, var item2Value: Int)

    class VerticalItemHolder(itemView: View, private val mAdapter: SimpleAdapter) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val mItem1Value: TextView
        private val mItem2Value: TextView
        private val mItem1Label: TextView
        private val mItem2Label: TextView

        init {
            itemView.setOnClickListener(this)

            mItem1Value = itemView.findViewById<View>(R.id.item_one_value) as TextView
            mItem2Value = itemView.findViewById<View>(R.id.item_two_value) as TextView
            mItem1Label = itemView.findViewById<View>(R.id.item_one_label) as TextView
            mItem2Label = itemView.findViewById<View>(R.id.item_two_label) as TextView
        }

        override fun onClick(v: View) {
            mAdapter.onItemHolderClick(this)
        }

        fun setItem1Value(homeScore: CharSequence) {
            mItem1Value.text = homeScore
        }

        fun setItem2Value(awayScore: CharSequence) {
            mItem2Value.text = awayScore
        }

        fun setItem1Label(homeName: CharSequence) {
            mItem1Label.text = homeName
        }

        fun setItem2Label(awayName: CharSequence) {
            mItem2Label.text = awayName
        }
    }

    companion object {

        fun generateDummyItem(): AdapterItem {
            val random = Random()
            return AdapterItem("Left", "Right",
                    random.nextInt(100),
                    random.nextInt(100))
        }

        fun generateDummyData(count: Int): List<AdapterItem> {
            val items = ArrayList<AdapterItem>()

            for (i in 0 until count) {
                items.add(SimpleAdapter.AdapterItem("Negative", "Positive", i, i + 5))
            }

            return items
        }
    }
}
