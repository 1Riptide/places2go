package com.caseystalnaker.places2go.adapters

import android.net.Uri
import android.support.annotation.ColorInt
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import com.caseystalnaker.places2go.R
import com.caseystalnaker.places2go.dto.WebcamDTO
import com.facebook.drawee.view.SimpleDraweeView


class WebcamsListAdapter(private val items: ArrayList<WebcamDTO>, @ColorInt private val liveColor: Int, @ColorInt private val defaultColor: Int) : RecyclerView.Adapter<WebcamsListAdapter.VerticalItemHolder>() {

    private val TAG = "WebcamsListAdapter"

    private var mOnItemClickListener: AdapterView.OnItemClickListener? = null

    fun getDataAtPosition(position: Int): WebcamDTO {
        return items[position]
    }

    /*
     * Inserting a new item at the head of the list. This uses a specialized
     * RecyclerView method, notifyItemInserted(), to trigger any enabled item
     * animations in addition to updating the view.
     */
    fun addItem(position: Int) {
        if (position > items.size) return

        items.add(position, items[position])
        notifyItemInserted(position)
    }

    /*
     * Inserting a new item at the head of the list. This uses a specialized
     * RecyclerView method, notifyItemRemoved(), to trigger any enabled item
     * animations in addition to updating the view.
     */
    fun removeItem(position: Int) {
        if (position >= items.size) return

        items.removeAt(position)
        notifyItemRemoved(position)
    }

    /**
     * Replace everything and notify
     */
    fun swap(@NonNull data: ArrayList<WebcamDTO>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(container: ViewGroup, viewType: Int): VerticalItemHolder {
        val inflater = LayoutInflater.from(container.context)
        val root = inflater.inflate(R.layout.webcam_snapshot_item, container, false)

        return VerticalItemHolder(root, this, liveColor, defaultColor)
    }

    override fun onBindViewHolder(itemHolder: VerticalItemHolder, position: Int) {

        val itemView = itemHolder.itemView
        if (position % 4 == 0) {
            val height = itemView.context.resources
                    .getDimensionPixelSize(R.dimen.card_staggered_height)
            itemView.minimumHeight = height
        } else {
            itemView.minimumHeight = 0
        }

        val item = items[position]
        try {
            item.player?.live?.available?.let { itemHolder.setCamStatus(it.toString()) }
            item.title?.let { itemHolder.setCamTitle(it) }
            item.image?.current?.toenail?.let { itemHolder.setCamImage(it) }
        } catch (e: NullPointerException) {
            Log.wtf(TAG, "onBindViewHolder: Your data is less-than good :( ", e);
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setOnItemClickListener(onItemClickListener: AdapterView.OnItemClickListener) {
        mOnItemClickListener = onItemClickListener
    }

    private fun onItemHolderClick(itemHolder: VerticalItemHolder) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener!!.onItemClick(null, itemHolder.itemView,
                    itemHolder.adapterPosition, itemHolder.itemId)
        }
    }

    class VerticalItemHolder(itemView: View, private val itemAdapter: WebcamsListAdapter, private val mLiveColor: Int, private val mDefaultColor: Int) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val mStatus: TextView
        private val mCamTitle: TextView
        private val mCamImage: SimpleDraweeView
        private val TAG = "VerticalItemHolder"

        init {
            itemView.setOnClickListener(this)

            mStatus = itemView.findViewById<View>(R.id.status) as TextView
            mCamTitle = itemView.findViewById<View>(R.id.cam_title) as TextView
            mCamImage = itemView.findViewById<View>(R.id.snapshot_image) as SimpleDraweeView

        }

        override fun onClick(v: View) {
            itemAdapter.onItemHolderClick(this)
        }

        fun setCamStatus(status: CharSequence) {
            if (status == true.toString()) {
                mStatus.setTextColor(mLiveColor);
            } else {
                mStatus.setTextColor(mDefaultColor)
            }
            mStatus.text = status
        }

        fun setCamTitle(title: CharSequence) {
            mCamTitle.text = title
        }

        fun setCamImage(@NonNull imageURL: CharSequence) {
            Log.d(TAG, "setCamImage:  to = " + imageURL)
            val uri = Uri.parse(imageURL.toString())
            mCamImage.setImageURI(uri)
        }

    }

}
