package com.caseystalnaker.places2go

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import com.caseystalnaker.places2go.Places2GoApplication.Companion.mMashapeHost
import com.caseystalnaker.places2go.Places2GoApplication.Companion.mMashapeKey
import com.caseystalnaker.places2go.activities.CamAPIActivity
import com.caseystalnaker.places2go.activities.CamBrowserActivity
import com.caseystalnaker.places2go.dto.WebcamDTO
import com.caseystalnaker.places2go.dto.WebcamResponseDTO
import com.caseystalnaker.places2go.dto.WebcamTypeDTO
import retrofit2.Response

class StartupActivity : CamAPIActivity() {
    override fun onCamListResponseReady(response: Response<WebcamResponseDTO>?) {
        launchCameraBrowser(response)
    }

    protected fun launchCameraBrowser(response: retrofit2.Response<WebcamResponseDTO>?) {
        // Array of all webcam data
        val webcamStreamList = response?.body()?.result?.webcams!!
        /*
        Lets work with ArrayLists of relevent data tho.
        These are containers to hold some of this data when we convert from Array to ArrayList.
         */
        val streamList: ArrayList<WebcamDTO> = ArrayList()
        val catsList: ArrayList<WebcamTypeDTO> = ArrayList()

        // Load home activity and finish.
        val intent = Intent(baseContext, CamBrowserActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        intent.putParcelableArrayListExtra(CamBrowserActivity.WEBCAM_CAT_DATA_KEY, mCamCatList.toCollection(catsList) as java.util.ArrayList<out Parcelable>)
        intent.putParcelableArrayListExtra(CamBrowserActivity.WEBCAM_STREAM_DATA_KEY, webcamStreamList.toCollection(streamList) as java.util.ArrayList<out Parcelable>)
        startActivity(intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_startup)
        Log.d(TAG, "onCreate: mCamAPI : " + mCamAPI + " mRetroFit : " + mRetrofit)

        // https://android.jlelse.eu/keddit-part-6-api-retrofit-kotlin-d309074af0
        val camCatsApiCall: retrofit2.Call<WebcamResponseDTO> = mCamAPI.getWebcamList(mMashapeKey, mMashapeHost, "en", "categories")

        // Get cam cat list from api
        fetchCategoryList(camCatsApiCall)
    }

    companion object {
        private const val TAG = "StartupActivity"
    }
}
