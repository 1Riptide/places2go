package com.caseystalnaker.places2go.interfaces

import com.caseystalnaker.places2go.dto.WebcamResponseDTO
import retrofit2.Call
import retrofit2.http.*

/**
 * Class contains mappings of API definitions to end points.
 *
 * @link https://rapidapi.com/webcams.travel/api/webcams.travel/docs

    Initially, the list contains all webcams. The resulting list of webcams from applying a modifier
     will be used as input for the following modifier. With every applied modifier, the list of
     webcams will be narrowed down.

    Examples

     * Use case: In your app, list the webcams that are around the current position of a user.
    /webcams/list/nearby=40.11,12.05,5/orderby=distance/limit=20
    List the first 20 webcams (limit=20) that are closest to the coordinates (40.11, 12.05) within a
     radius of 5km (nearby=40.11,12.05,5).

     * Use case: A directory of the most popular beaches in Italy.
    /webcams/list/category=beach/country=IT/orderby=popularity,desc/limit=20
    List the first 20 webcams (limit=20) that are in the category "beach" (category=beach) and in
    Italy (country=IT), ordered by their popularity (orderby=popularity).

     * Use case: Show the webcam of the beach that is closest to the current position of a user of
    your app.
    /webcams/list/nearby=40.11,12.05,5/category=beach/orderby=distance/limit=1
    List the closest webcam (orderby=distance and limit=1) to the coordinates (40.11, 12.05) within
    a radius of 5km (nearby=40.11,12.05,5) and that is in
    the category "beach" (category=beach).

    Parameters
    Please refer to the description of the various modifiers to learn more about their usage and
    defaults.

    The modifiers are:

    Explicit modifiers
    bbox	        List of webcams in an area.
    category	    List of webcams in a category.
    continent	    List of webcams in a continent.
    country	        List of webcams in a country.
    nearby	        List of webcams around a position.
    property	    List of webcams with a certain property.
    region	        List of webcams in a region.
    webcam	        Explicit list of webcams.

    Implicit modifiers
    exclude	        Exclude specific webcams from a list of webcams.
    orderby	        Order the list of webcams.
    limit	        Slice the list of webcams.

    Usage
        The explicit modifiers are applied in the order as given in the API call. You may apply up
        to 10 explicit modifiers.The implicit modifiers are always applied only once in this order
        and after any given explicit modifier (even if none are given).If no explicit modifier is
        given then all available webcams are in the list before applying the implicit modifiers.

    Categories
    To retrieve a list with all available categories with:

    /webcams/list?show=categories
    The currently defined categories are:

        area (Alue)
        square (Aukio/kuja)
        golf (Golfkenttä)
        indoor (Julkinen sisätila)
        traffic (Katu/liikenne)
        city (Kaupunki)
        bay (Lahti)
        lake (Lake/River)
        camping (Leirintäalue)
        airport (Lentokenttä)
        resort (Lomakeskus)
        marketplace (Marketplace)
        forest (Metsä)
        other (Other)
        park (Puisto)
        building (Rakennus)
        coast (Rannikko)
        beach (Ranta)
        island (Saari)
        harbor (Satama)
        meteo (Taivas)
        pool (Uima-allas)
        landscape (Ulkona)
        underwater (Underwater)
        sportarea (Urheilualue)
        water (Vesi)
        mountain (Vuori/kanjoni)
*/

interface WebcamAPIInterface {
    // contract : webcams/list/{modifier}[/{modifier}[/...]]
    // example: webcams/list/limit%3D200?lang=en&show=webcams%3Aimage%2Clocation%2Curl")

    @GET("/webcams/list/limit=50?")
    fun getWebcamList(@Header("X-Mashape-Key") key: String,
                      @Header("X-Mashape-Host") host: String,
            //@Query("bbox")bbox: String,
                      @Query("lang") lang: String,
                      @Query("show") show: String):
            Call<WebcamResponseDTO>

    // contract: webcams/list/category={category}[,{category}[,...]]
    // "https://webcamstravel.p.mashape.com/webcams/list/category=coast?show=webcams%3Aimage%2Clocation&lang=en"

    @GET("/webcams/list/category={category}/limit=50?")
    fun getWebcamsByCategory(@Header("X-Mashape-Key") key: String,
                             @Header("X-Mashape-Host") host: String,
                             @Path("category") category: String,
                             @Query("lang") lang: String,
                             @Query("show") show: String):
            Call<WebcamResponseDTO>

    //
    @GET("/webcams/list/category={category}/limit=50?")
    fun getWebcamsByProperty(@Header("X-Mashape-Key") key: String,
                             @Header("X-Mashape-Host") host: String,
                             @Path("property") category: String,
                             @Query("lang") lang: String,
                             @Query("show") show: String):
            Call<WebcamResponseDTO>

    // example : webcams/list?show=webcams%3Aimage%2Clocation&lang=en&bbox=41.257160,-95.995102,36.778259,-120.457939
    @GET("/webcams/list/category={category}/limit=50?")
    fun getWebcamsByBoundingBox(@Header("X-Mashape-Key") key: String,
                                @Header("X-Mashape-Host") host: String,
                                @Path("bbox") category: String,
                                @Query("lang") lang: String,
                                @Query("show") show: String):
            Call<WebcamResponseDTO>

}