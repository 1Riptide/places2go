package com.caseystalnaker.places2go.widgets

import android.content.Context
import android.util.AttributeSet
import android.widget.GridLayout
import kotlinx.android.synthetic.main.webcam_snapshot_item.view.*

class WebcamSnapshotView : GridLayout {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun toString(): String {
        return (cam_title!!.text.toString()
                + ": " + left + "," + top
                + ": " + measuredWidth + "x" + measuredHeight)
    }
}
