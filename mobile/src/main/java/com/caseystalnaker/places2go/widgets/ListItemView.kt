package com.caseystalnaker.places2go.widgets

import android.content.Context
import android.util.AttributeSet
import android.widget.GridLayout
import kotlinx.android.synthetic.main.double_label_item.view.*

class ListItemView : GridLayout {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun onFinishInflate() {
        super.onFinishInflate()
    }

    override fun toString(): String {
        return (item_two_value!!.text.toString() + "v" + item_one_value!!.text
                + ": " + left + "," + top
                + ": " + measuredWidth + "x" + measuredHeight)
    }
}
