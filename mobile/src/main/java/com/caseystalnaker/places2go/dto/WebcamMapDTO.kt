package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/
 * @see https://developers.webcams.travel/
 *
 *
   Example response
   {
      "clustersize": 127
   }
 */
data class WebcamMapDTO(var clustersize: Int) : Parcelable {
   constructor(parcel: Parcel) : this(
           parcel.readInt()) {
   }

   override fun writeToParcel(parcel: Parcel, flags: Int) {
      parcel.writeInt(clustersize)
   }

   override fun describeContents(): Int {
      return 0
   }

   companion object CREATOR : Parcelable.Creator<WebcamMapDTO> {
      override fun createFromParcel(parcel: Parcel): WebcamMapDTO {
         return WebcamMapDTO(parcel)
      }

      override fun newArray(size: Int): Array<WebcamMapDTO?> {
         return arrayOfNulls(size)
      }
   }

}