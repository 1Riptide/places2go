package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.Nullable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/{modifier}[/{modifier}[/...]]
 * @see https://developers.webcams.travel/
 *
 Example Response:
{
    "city":"Beinwil am See",
    "region":"Canton of Aargau",
    "region_code":"CH.AG",
    "country":"Switzerland",
    "country_code":"CH",
    "continent":"Europe",
    "continent_code":"EU",
    "latitude":47.260586,
    "longitude":8.205056,
    "timezone":"Europe/Zurich",
    "wikipedia":""
}

 */
data class LocationDTO(@Nullable var city: String? = null,
                       @Nullable var region: String? = null,
                       @Nullable var region_code: String? = null,
                       @Nullable var country: String? = null,
                       @Nullable var country_code: String? = null,
                       @Nullable var continent: String? = null,
                       @Nullable var continent_code: String? = null,
                       @Nullable var latitude: Double? = null,
                       @Nullable var longitude: Double? = null,
                       @Nullable var timezone: String? = null,
                       @Nullable var wikipedia: String? = null) : Parcelable {

    constructor(parcel: Parcel?) : this(
            parcel?.readString(),
            parcel?.readString(),
            parcel?.readString(),
            parcel?.readString(),
            parcel?.readString(),
            parcel?.readString(),
            parcel?.readString(),
            parcel?.readDouble(),
            parcel?.readDouble(),
            parcel?.readString(),
            parcel?.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        city?.let{parcel.writeString(it)}
        region?.let{parcel.writeString(it)}
        region_code?.let{parcel.writeString(it)}
        country?.let{parcel.writeString(it)}
        country_code?.let{parcel.writeString(it)}
        continent?.let{parcel.writeString(it)}
        continent_code?.let{parcel.writeString(it)}
        latitude?.let{parcel.writeDouble(it)}
        longitude?.let{parcel.writeDouble(it)}
        timezone?.let{parcel.writeString(it)}
        wikipedia?.let{parcel.writeString(it)}
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LocationDTO> {
        override fun createFromParcel(parcel: Parcel): LocationDTO {
            return LocationDTO(parcel)
        }

        override fun newArray(size: Int): Array<LocationDTO?> {
            return arrayOfNulls(size)
        }
    }
}