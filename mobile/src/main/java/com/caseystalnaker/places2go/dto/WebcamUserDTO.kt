package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.NonNull
import android.support.annotation.Nullable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/
 * @see https://developers.webcams.travel/
 *
 *
   Example response
   {
      "id": "3626",
      "name": "ingo",
      "url": "http://www.webcams.travel/user/3626"
   }
 */
data class WebcamUserDTO(@NonNull var id: Int,
                         @Nullable var name: String? = null,
                         @Nullable var url: String? = null) : Parcelable {
   constructor(parcel: Parcel) : this(
           parcel.readInt(),
           parcel.readString(),
           parcel.readString()) {
   }

   override fun writeToParcel(parcel: Parcel, flags: Int) {
      parcel.writeInt(id)
      name?.let{parcel.writeString(name)}
      url?.let{parcel.writeString(url)}
   }

   override fun describeContents(): Int {
      return 0
   }

   companion object CREATOR : Parcelable.Creator<WebcamUserDTO> {
      override fun createFromParcel(parcel: Parcel): WebcamUserDTO {
         return WebcamUserDTO(parcel)
      }

      override fun newArray(size: Int): Array<WebcamUserDTO?> {
         return arrayOfNulls(size)
      }
   }

}