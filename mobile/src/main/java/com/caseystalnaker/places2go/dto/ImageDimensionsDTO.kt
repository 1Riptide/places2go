package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/
 * @see https://developers.webcams.travel/
 *
 *
   Example response
   {
      "width":200,
      "height":112
   }
 */
data class ImageDimensionsDTO(var width: Int,
                              var height: Int) : Parcelable {
   constructor(parcel: Parcel) : this(
           parcel.readInt(),
           parcel.readInt()) {
   }

   override fun writeToParcel(parcel: Parcel, flags: Int) {
      parcel.writeInt(width)
      parcel.writeInt(height)
   }

   override fun describeContents(): Int {
      return 0
   }

   companion object CREATOR : Parcelable.Creator<ImageDimensionsDTO> {
      override fun createFromParcel(parcel: Parcel): ImageDimensionsDTO {
         return ImageDimensionsDTO(parcel)
      }

      override fun newArray(size: Int): Array<ImageDimensionsDTO?> {
         return arrayOfNulls(size)
      }
   }

}