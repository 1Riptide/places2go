package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/{modifier}[/{modifier}[/...]]
 * @see https://developers.webcams.travel/
 *
 Example Response

    {
        "current":{
            "icon":"https://images.webcams.travel/icon/1000550952.jpg",
            "thumbnail":"https://images.webcams.travel/thumbnail/1000550952.jpg",
            "preview":"https://images.webcams.travel/preview/1000550952.jpg",
            "toenail":"https://images.webcams.travel/thumbnail/1000550952.jpg"
        },
        "daylight":{
            "icon":"https://images.webcams.travel/daylight/icon/1000550952.jpg",
            "thumbnail":"https://images.webcams.travel/daylight/thumbnail/1000550952.jpg",
            "preview":"https://images.webcams.travel/daylight/preview/1000550952.jpg",
            "toenail":"https://images.webcams.travel/daylight/thumbnail/1000550952.jpg"
        },
        "sizes":{
                "icon":{
                "width":48,
                "height":48
            },
            "thumbnail":{
                "width":200,
                "height":112
            },
            "preview":{
                "width":400,
                "height":224
            },
            "toenail":{
                "width":200,
                "height":112
            }
        },
        "update":1524398672
    }

 */
data class WebcamImageDTO(var current: ImageURLsDTO,
                          var daylight: ImageURLsDTO,
                          var sizes: ImageSizesDTO,
                          var update: Long ): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(ImageURLsDTO::class.java.classLoader),
            parcel.readParcelable(ImageURLsDTO::class.java.classLoader),
            parcel.readParcelable(ImageSizesDTO::class.java.classLoader),
            parcel.readLong()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(current, flags)
        parcel.writeParcelable(daylight, flags)
        parcel.writeParcelable(sizes, flags)
        parcel.writeLong(update)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WebcamImageDTO> {
        override fun createFromParcel(parcel: Parcel): WebcamImageDTO {
            return WebcamImageDTO(parcel)
        }

        override fun newArray(size: Int): Array<WebcamImageDTO?> {
            return arrayOfNulls(size)
        }
    }
}