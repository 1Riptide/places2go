package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/
 * @see https://developers.webcams.travel/
 *
 *
   Example response
   {
      "views": 127
   }
 */
data class WebcamStatisticsDTO(var views: Int) : Parcelable {
   constructor(parcel: Parcel) : this(
           parcel.readInt()) {
   }

   override fun writeToParcel(parcel: Parcel, flags: Int) {
      parcel.writeInt(views)
   }

   override fun describeContents(): Int {
      return 0
   }

   companion object CREATOR : Parcelable.Creator<WebcamStatisticsDTO> {
      override fun createFromParcel(parcel: Parcel): WebcamStatisticsDTO {
         return WebcamStatisticsDTO(parcel)
      }

      override fun newArray(size: Int): Array<WebcamStatisticsDTO?> {
         return arrayOfNulls(size)
      }
   }

}