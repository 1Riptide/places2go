package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.Nullable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/
 * @see https://developers.webcams.travel/
 *
 *
   Example response
   {
      "status": "OK",
      "result": {
      "offset": 0,
      "limit": 10,
      "total": 1,
      "webcams": [ ... ],
      "categories": [ ... ],
      "properties": [ ... ],
      "continents": [ ... ],
      "countries": [ ... ],
      "regions": [ ... ]
   }
 */
data class WebcamResponseDTO(@Nullable var status: String? = null,
                             @Nullable var result: WebcamListDTO? = null) : Parcelable {
   constructor(parcel: Parcel) : this(
           parcel.readString(),
           parcel.readParcelable(WebcamListDTO::class.java.classLoader)) {
   }

   override fun writeToParcel(parcel: Parcel, flags: Int) {
      parcel.writeString(status)
      parcel.writeParcelable(result, flags)
   }

   override fun describeContents(): Int {
      return 0
   }

   companion object CREATOR : Parcelable.Creator<WebcamResponseDTO> {
      override fun createFromParcel(parcel: Parcel): WebcamResponseDTO {
         return WebcamResponseDTO(parcel)
      }

      override fun newArray(size: Int): Array<WebcamResponseDTO?> {
         return arrayOfNulls(size)
      }
   }
}