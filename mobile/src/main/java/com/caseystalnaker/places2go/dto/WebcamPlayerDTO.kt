package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.Nullable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/
 * @see https://developers.webcams.travel/
 *
 *
Example response
{
    "live": {
        "available": true,
        "embed": "https://api.lookr.com/embed/player/1485691420/live"
    },
    "day": {
        "available": true,
        "link": "https://www.lookr.com/lookout/1485691420#action-play-day",
        "embed": "https://api.lookr.com/embed/player/1485691420/day"
    },
    "month": {
        "available": true,
        "link": "https://www.lookr.com/lookout/1485691420#action-play-month",
        "embed": "https://api.lookr.com/embed/player/1485691420/month"
    },
    "year": {
        "available": true,
        "link": "https://www.lookr.com/lookout/1485691420#action-play-year",
        "embed": "https://api.lookr.com/embed/player/1485691420/year"
    },
    "lifetime": {
        "available": true,
        "link": "https://www.lookr.com/lookout/1485691420#action-play-lifetime",
        "embed": "https://api.lookr.com/embed/player/1485691420/lifetime"
    }
}

 */
data class WebcamPlayerDTO(@Nullable var live: WebcamPlayerTypeDTO? = null,
                           @Nullable var day: WebcamPlayerTypeDTO? = null,
                           @Nullable var night: WebcamPlayerTypeDTO? = null,
                           @Nullable var year: WebcamPlayerTypeDTO? = null,
                           @Nullable var lifetime: WebcamPlayerTypeDTO? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(WebcamPlayerTypeDTO::class.java.classLoader),
            parcel.readParcelable(WebcamPlayerTypeDTO::class.java.classLoader),
            parcel.readParcelable(WebcamPlayerTypeDTO::class.java.classLoader),
            parcel.readParcelable(WebcamPlayerTypeDTO::class.java.classLoader),
            parcel.readParcelable(WebcamPlayerTypeDTO::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(live, flags)
        parcel.writeParcelable(day, flags)
        parcel.writeParcelable(night, flags)
        parcel.writeParcelable(year, flags)
        parcel.writeParcelable(lifetime, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WebcamPlayerDTO> {
        override fun createFromParcel(parcel: Parcel): WebcamPlayerDTO {
            return WebcamPlayerDTO(parcel)
        }

        override fun newArray(size: Int): Array<WebcamPlayerDTO?> {
            return arrayOfNulls(size)
        }
    }

}