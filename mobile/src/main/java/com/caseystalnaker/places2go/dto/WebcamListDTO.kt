package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.NonNull

/**
 * Mapping for JSON response from :
 * GET/webcams/list/{modifier}[/{modifier}[/...]]
 * @see https://developers.webcams.travel/
 *
Example response
region = CH.AG
GET/webcams/list/region={region}[,{region}[,...]]

{
"offset": 0,
"limit": 10,
"total": 1,
"webcams": [ ... ],
"categories": [ ... ],
"properties": [ ... ],
"continents": [ ... ],
"countries": [ ... ],
"regions": [ ... ]
}
 */
data class WebcamListDTO(@NonNull var offset: Int? = 0,
                         @NonNull var limit: Int? = 0,
                         @NonNull var total: Int? = 0,
                         @NonNull var webcams: Array<WebcamDTO>? = emptyArray(),
                         @NonNull var categories: Array<WebcamTypeDTO>? = emptyArray(),
                         @NonNull var properties: Array<WebcamTypeDTO>? = emptyArray(),
                         @NonNull var continents: Array<WebcamTypeDTO>? = emptyArray(),
                         @NonNull var countries: Array<WebcamTypeDTO>? = emptyArray(),
                         @NonNull var regions: Array<WebcamTypeDTO>? = emptyArray()) : Parcelable {
    constructor(parcel: Parcel?) : this(
            parcel?.readInt(),
            parcel?.readInt(),
            parcel?.readInt(),
            parcel?.createTypedArray(WebcamDTO),
            parcel?.createTypedArray(WebcamTypeDTO),
            parcel?.createTypedArray(WebcamTypeDTO),
            parcel?.createTypedArray(WebcamTypeDTO),
            parcel?.createTypedArray(WebcamTypeDTO),
            parcel?.createTypedArray(WebcamTypeDTO)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        offset?.let { parcel.writeInt(it) }
        limit?.let { parcel.writeInt(it) }
        total?.let { parcel.writeInt(it) }
        webcams?.let { parcel.writeTypedArray(webcams, flags) }
        categories?.let { parcel.writeTypedArray(categories, flags) }
        properties?.let { parcel.writeTypedArray(properties, flags) }
        continents?.let { parcel.writeTypedArray(continents, flags) }
        countries?.let { parcel.writeTypedArray(countries, flags) }
        regions?.let { parcel.writeTypedArray(regions, flags) }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WebcamListDTO> {
        override fun createFromParcel(parcel: Parcel): WebcamListDTO {
            return WebcamListDTO(parcel)
        }

        override fun newArray(size: Int): Array<WebcamListDTO?> {
            return arrayOfNulls(size)
        }
    }

}