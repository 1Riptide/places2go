package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.NonNull
import android.support.annotation.Nullable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/
 * @see https://developers.webcams.travel/
 *
 *
   Example response
   {
      "desktop": "http://www.webcams.travel/webcam/1171032474-Weather-Hardbr%C3%BCcke-Zurich",
      "mobile": "http://m.webcams.travel/webcam/1171032474-Weather-Hardbr%C3%BCcke-Zurich"
   }

 */
data class WebcamURLTypeDTO(@NonNull var desktop: String? = null,
                            @Nullable var mobile: String? = null) : Parcelable {
   constructor(parcel: Parcel) : this(
           parcel.readString(),
           parcel.readString()) {
   }

   override fun writeToParcel(parcel: Parcel, flags: Int) {
      parcel.writeString(desktop)
      parcel.writeString(mobile)
   }

   override fun describeContents(): Int {
      return 0
   }

   companion object CREATOR : Parcelable.Creator<WebcamURLTypeDTO> {
      override fun createFromParcel(parcel: Parcel): WebcamURLTypeDTO {
         return WebcamURLTypeDTO(parcel)
      }

      override fun newArray(size: Int): Array<WebcamURLTypeDTO?> {
         return arrayOfNulls(size)
      }
   }

}