package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.NonNull
import android.support.annotation.Nullable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/
 * @see https://developers.webcams.travel/
 *
 *
   Example response
   {
      "available": true,
      "link": "https://www.lookr.com/lookout/1485691420#action-play-day",
      "embed": "https://api.lookr.com/embed/player/1485691420/day"
   }

 */
data class WebcamPlayerTypeDTO(@NonNull var available: Boolean? = false,
                               @Nullable var link: String? = null,
                               @Nullable var embed: String? = null) : Parcelable {
   constructor(parcel: Parcel) : this(
           parcel.readValue(Boolean::class.java.classLoader) as Boolean?,
           parcel.readString(),
           parcel.readString()) {
   }

   override fun writeToParcel(parcel: Parcel, flags: Int) {
      parcel.writeValue(available)
      parcel.writeString(link)
      parcel.writeString(embed)
   }

   override fun describeContents(): Int {
      return 0
   }

   companion object CREATOR : Parcelable.Creator<WebcamPlayerTypeDTO> {
      override fun createFromParcel(parcel: Parcel): WebcamPlayerTypeDTO {
         return WebcamPlayerTypeDTO(parcel)
      }

      override fun newArray(size: Int): Array<WebcamPlayerTypeDTO?> {
         return arrayOfNulls(size)
      }
   }

}