package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/{modifier}[/{modifier}[/...]]
 * @see https://developers.webcams.travel/

 Example Response:
    {
        "icon":"https://images.webcams.travel/icon/1000550952.jpg",
        "thumbnail":"https://images.webcams.travel/thumbnail/1000550952.jpg",
        "preview":"https://images.webcams.travel/preview/1000550952.jpg",
        "toenail":"https://images.webcams.travel/thumbnail/1000550952.jpg"
    }
 */
data class ImageURLsDTO(var icon: String,
                        var thumbnail: String,
                        var preview: String,
                        var toenail: String ): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(icon)
        parcel.writeString(thumbnail)
        parcel.writeString(preview)
        parcel.writeString(toenail)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ImageURLsDTO> {
        override fun createFromParcel(parcel: Parcel): ImageURLsDTO {
            return ImageURLsDTO(parcel)
        }

        override fun newArray(size: Int): Array<ImageURLsDTO?> {
            return arrayOfNulls(size)
        }
    }
}