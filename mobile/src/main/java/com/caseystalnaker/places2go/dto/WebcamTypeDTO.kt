package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/
 * @see https://developers.webcams.travel/
 *
 *
   Example response
   {
      "id": "hd",
      "name": "HD",
      "count": 25
   }
 */
data class WebcamTypeDTO(var id: String? = null,
                         var name: String? = null,
                         var count: Int? = 0) : Parcelable {
   constructor(parcel: Parcel?) : this(
           parcel?.readString(),
           parcel?.readString(),
           parcel?.readInt()) {
   }

   override fun writeToParcel(parcel: Parcel, flags: Int) {
      id?.let{parcel.writeString(it)}
      name?.let{parcel.writeString(it)}
      count?.let{parcel.writeInt(it)}
   }

   override fun describeContents(): Int {
      return 0
   }

   companion object CREATOR : Parcelable.Creator<WebcamTypeDTO> {
      override fun createFromParcel(parcel: Parcel): WebcamTypeDTO {
         return WebcamTypeDTO(parcel)
      }

      override fun newArray(size: Int): Array<WebcamTypeDTO?> {
         return arrayOfNulls(size)
      }
   }

}