package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.Nullable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/
 * @see https://developers.webcams.travel/
 *
 *
Example response
{
    "current": {
        "desktop": "http://www.webcams.travel/webcam/1171032474-Weather-Hardbr%C3%BCcke-Zurich",
        "mobile": "http://m.webcams.travel/webcam/1171032474-Weather-Hardbr%C3%BCcke-Zurich"
    },
    "daylight": {
        "desktop": "http://www.webcams.travel/webcam/1171032474-Weather-Hardbr%C3%BCcke-Zurich/daylight",
        "mobile": "http://m.webcams.travel/webcam/1171032474-Weather-Hardbr%C3%BCcke-Zurich/daylight"
    },
    "edit": "https://lookr.com/edit/1171032474"
}

 */
data class WebcamURLDTO(@Nullable var current: WebcamURLTypeDTO? = null,
                        @Nullable var daylight: WebcamURLTypeDTO? = null,
                        @Nullable var edit: String? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(WebcamURLTypeDTO::class.java.classLoader),
            parcel.readParcelable(WebcamURLTypeDTO::class.java.classLoader),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(current, flags)
        parcel.writeParcelable(daylight, flags)
        parcel.writeString(edit)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WebcamURLDTO> {
        override fun createFromParcel(parcel: Parcel): WebcamURLDTO {
            return WebcamURLDTO(parcel)
        }

        override fun newArray(size: Int): Array<WebcamURLDTO?> {
            return arrayOfNulls(size)
        }
    }

}