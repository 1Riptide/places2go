package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.Nullable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/{modifier}[/{modifier}[/...]]
 * @see https://developers.webcams.travel/
 *
 Example Response:

    {
        "id": "1171032474",
        "status": "active",
        "title": "Zurich › North: Best Carwash − Hardbrücke − Jumbo compact Zürich − Hard One − Abaton − KV Zürich Business School − Escher Wyss",
        "category": <webcam.category object>,
        "image": <webcam.image object>,
        "location": <webcam.location object>,
        "map": <webcam.map object>,
        "player": <webcam.player object>,
        "property": <webcam.property object>,
        "statistics": <webcam.statistics object>,
        "url": <webcam.url object>,
        "user": <webcam.user object>
    }
 */

data class WebcamDTO(@Nullable var id: Int? = null,
                     @Nullable var status: String? = null,
                     @Nullable var title: String? = null,
                     @Nullable var image: WebcamImageDTO?,
                     @Nullable var location: LocationDTO? = null,
                     @Nullable var map: WebcamMapDTO? = null,
                     @Nullable var player: WebcamPlayerDTO? = null,
                     @Nullable var property: Array<WebcamTypeDTO>? = emptyArray(),
                     @Nullable var statistics: WebcamStatisticsDTO? = null,
                     @Nullable var url: WebcamURLDTO? = null,
                     @Nullable var user: WebcamUserDTO? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(WebcamImageDTO::class.java.classLoader),
            parcel.readParcelable(LocationDTO::class.java.classLoader),
            parcel.readParcelable(WebcamMapDTO::class.java.classLoader),
            parcel.readParcelable(WebcamPlayerDTO::class.java.classLoader),
            parcel.createTypedArray(WebcamTypeDTO),
            parcel.readParcelable(WebcamStatisticsDTO::class.java.classLoader),
            parcel.readParcelable(WebcamURLDTO::class.java.classLoader),
            parcel.readParcelable(WebcamUserDTO::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        id?.let { parcel.writeInt(it) }
        parcel.writeString(status)
        parcel.writeString(title)
        parcel.writeParcelable(image, flags)
        parcel.writeParcelable(location, flags)
        parcel.writeParcelable(map, flags)
        parcel.writeParcelable(player, flags)
        parcel.writeTypedArray(property, flags)
        parcel.writeParcelable(statistics, flags)
        parcel.writeParcelable(url, flags)
        parcel.writeParcelable(user, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WebcamDTO> {
        override fun createFromParcel(parcel: Parcel): WebcamDTO {
            return WebcamDTO(parcel)
        }

        override fun newArray(size: Int): Array<WebcamDTO?> {
            return arrayOfNulls(size)
        }
    }

}
