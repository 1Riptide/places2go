package com.caseystalnaker.places2go.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * Mapping for JSON response from :
 * GET/webcams/list/{modifier}[/{modifier}[/...]]
 * @see https://developers.webcams.travel/

 Example Response:
    {
        "icon":{
            "width":48,
            "height":48
        },
        "thumbnail":{
            "width":200,
            "height":112
        },
        "preview":{
            "width":400,
            "height":224
        },
        "toenail":{
            "width":200,
            "height":112
        }
    }
 */
data class ImageSizesDTO(var icon: ImageDimensionsDTO,
                         var thumbnail: ImageDimensionsDTO,
                         var preview: ImageDimensionsDTO,
                         var toenail: ImageDimensionsDTO): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(ImageDimensionsDTO::class.java.classLoader),
            parcel.readParcelable(ImageDimensionsDTO::class.java.classLoader),
            parcel.readParcelable(ImageDimensionsDTO::class.java.classLoader),
            parcel.readParcelable(ImageDimensionsDTO::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(icon, flags)
        parcel.writeParcelable(thumbnail, flags)
        parcel.writeParcelable(preview, flags)
        parcel.writeParcelable(toenail, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ImageSizesDTO> {
        override fun createFromParcel(parcel: Parcel): ImageSizesDTO {
            return ImageSizesDTO(parcel)
        }

        override fun newArray(size: Int): Array<ImageSizesDTO?> {
            return arrayOfNulls(size)
        }
    }
}