package com.caseystalnaker.places2go.decorators

import android.content.Context
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import com.caseystalnaker.places2go.R

/**
 * ItemDecoration implementation that applies an inset margin
 * around each child of the RecyclerView. The inset value is controlled
 * by a dimension resource.
 */
class InsetDecoration(context: Context) : RecyclerView.ItemDecoration() {

    private val mInsets: Int

    init {
        mInsets = context.resources.getDimensionPixelSize(R.dimen.card_insets)
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State?) {
        outRect.set(mInsets, mInsets, mInsets, mInsets)
    }
}
